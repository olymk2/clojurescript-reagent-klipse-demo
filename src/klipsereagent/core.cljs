(ns klipsereagent.core
  (:require [reagent.core :as reagent]
            [cljfmt :refer [reformat-string]]
            [cljs.tools.reader :refer [read-string]]
            [cljs.js :refer [eval-str empty-state js-eval]]
            [ajax.core :refer [GET, POST]]
            ))

(def res (atom ""))
(def errors (atom ""))
(def stylesheets [{:name "tachyon"
                   :title "Tachyon CSS"
                   :link "https://unpkg.com/tachyons@4.10.0/css/tachyons.min.css"}
                  {:name "tailwind"
                   :title "Tailwind"
                   :link "https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css"}
                  {:name "bulma"
                   :title "Bulma"
                   :link "https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.min.css"}])


(defn request-url [& rest]
  (str "http:127.0.0.1:3001/api/" (apply str rest)))

(defn handle-failure [{:keys [status status-text response]}]
  (case status
    500 (do (reset! errors ["Something bad happened talking to the api"]))
    401 (do (reset! errors ["Not authorized"]))
    404 (do (reset! errors ["Could not find api endpoint"]))
    (swap! errors [(str (:msg response) (:msg response))])))


(defn fetch-data
  ([block]
   (GET (request-url (str "snippet/2"))
     {:params {}
      :headers {"Access-Control-Allow-Headers" "Content-Type"
                "Access-Control-Allow-Origin" "*"}
      :handler (fn [response] (reset! block response))
      :error-handler handle-failure
      :response-format :json
      :keywords? true})))



(defn eval-code-block [cljs-string evalresult]
  (eval-str (empty-state)
            (str "(ns cljs.user
                    (:refer-clojure :exclude [atom])
                    (:require reagent.core))

                  (def atom reagent.core/atom) "
                 (or (not-empty cljs-string)
                     " [:div]"))
            "code block"
            {:ns 'cljs.user
             :eval js-eval
             :static-fns true
             :source-map true
             :verbose true
             :def-emits-var false
             :load (fn [name cb] (cb {:lang :clj :source ""}))
             :context :statement}
            (fn [result]
              (reset! evalresult (:value result)))))

(defn chip-item [link tag]
  [:li.dib.mr1.mb2 [:a.f6.f5-ns.b.db.pa2.link.dim.dark-gray.ba.b--black-20 {:href link} tag]])

(defn chips [items]
  (into [:ul.p2.m0]
        (map (fn [{:keys [tag link]} item] (chip-item link tag)) items)))

(defn sidebar [links]
  [:div.absolute.t0.b0.l0.pa2.w5.h-100.bg-green
   [:article.pa2
    [:h1.f4.bold.center.mw6 "Select Framework"]
    (into [:ul.list.pl0.ml0.center.mw6.ba.b--light-silver.br2]
          (map (fn [{:keys [title link]} item]
                 [:li.ph3.pv3.bb.b--light-silver [:a.link.no-underline {:href link} title]]) links))]])

(defn code-wrapper [title code-test]
  (let [hic (reagent/atom "evaluating")]
    (reagent/create-class
     {:display-name "code wrapper"
      :component-did-mount (fn []
                             (eval-code-block code-test hic)
                             (.log js/console "+++++++++++++++++++")
                             (.log js/console code-test))
                             ;;(.log js/console (:value (eval-code-block code-test hic))))
      :reagent-render (fn [title code-test]
                        [:article.center.mw5.mw6-ns.br3.hidden.ba.b--black-10.mv4
                         [:h1.f4.bg-near-white.br3.br--top.black-60.mv0.pv2.ph3 title]
                         [chips [{:tag "tachyon" :link "1"} {:tag "card" :link "2"}]]
                         [:blockquote.athelas.ml0.mt0.pl4.black-90.bl.bw2.b--blue
                          [:p.f5.f4-m.f3-l.lh-copy.measure.mt0 code-test]
                          [:cite.f6.ttu.tracked.fs-normal]]
                         [:div @hic]])})))

(defn cljs-snippet [snippet]
  (clear)
  (let [code-test "[:div.bg-green \"hi\"]" result (atom "waiting...")]
    (.log js/console (eval-code-block code-test result))

    [:div "..." @result]
    [:div  (reagent/render-component (fn [] @result) (.getElementById js/document "app2"))]))
  ;;  ))

;; (defn filter-loaded [scripts]
;;   (reduce (fn [acc [loaded? src]]
;;             (if (loaded?) acc (conj acc src)))
;;           []
;;           scripts))

;; (defn klipse-wrapper
;;   [{:keys [scripts content settings]}]
;;   (let [loaded? (reagent/atom false)]
;;     (reagent/create-class
;;      {:component-did-mount (fn [_]
                             ;; (set! (.-klipse_settings js/window) (clj->js settings))
                             ;; (let [not-loaded (clj->js (map  #(-> % .toString goog.html.legacyconversions/trustedResourceUrlFromString) (filter-loaded scripts)))]
                             ;;   (.then
                             ;;    (jsl/safeLoadMany not-loaded)
                             ;;    #(do (js/console.info "Loaded:" not-loaded)
                             ;;         (reset! loaded? true))))
       ;;                              )
      ;; :reagent-render      (fn [{:keys [content]}]
      ;;                        content)})))

;; (defn klipse-snippet []
;;   [klipse-wrapper {:scripts {#(exists? js/klipse) "https://storage.googleapis.com/app.klipse.tech/plugin/js/klipse_plugin.js"}
;;                    :content [:div
;;                              [:div.klipse.language-klipse "(+ 3 3)"]
;;                              [:div.klipse.language-klipse "(require '[reagent.core :as r])"]

;;                              [:div.klipse.language-reagent
;;                               {:dataExternalLibs "https://github.com/reagent-project/reagent.git"
;;                               :data-external-libs "https://github.com/reagent-project/reagent.git"
;;                                :dataPreamble "(require '[reagent.core :as r])"}
;;                              "[:div \"hello world!\"]"]]
;;                    :settings {:selector ".language-klipse"}}])

(defn snippet []
  [:div
   [:div.ma3
    [:div.language-klipse.klipse-eval-clojure "(require '[reagent.core :as r])"]]
   [:div.ma3
    [:div.ma3.language-klipse.klipse-snippet "(+ 1 1 1)"]]
   [:div.ma3
    [:div.ma3.language-reagent
     {:data-external-libs "https://github.com/reagent-project/reagent.git"}
     "[:div \"hello world\"]"]]])

(def code-test-1 "[:article.center.mw5.mw6-ns.br3.hidden.ba.b--black-10.mv4 [:h1.f4.bg-near-white.br3.br--top.black-60.mv0.pv2.ph3 \"Title of card\"] [:div.pa3.bt.b--black-10 [:p.f6.f5-ns.lh-copy.measure \"
      Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod
      tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At
      vero eos et accusam et justo duo dolores et ea rebum.
    \"]]]")

(defn test-page []
  [:div
   [sidebar stylesheets]
   [:link {:rel "stylesheet" :type "text/css" :href (:tachyon stylesheets)}]
   [:div [code-wrapper "Sample 1" "[:div.bg-blue \"blue div\"]"]]
   [:div [code-wrapper "Sample 2" "[:div.bg-green \"green div\"]"]]
   [:div [code-wrapper "Sample 3" code-test-1]]])

(defn mount-root []
  (reagent/render [:div [test-page] "test"] (.getElementById js/document "app")))


;;(replumb-str "(require '[reagent.core :as r]) [:div \"hello\"]")


(mount-root)

;; remove klipse from index.html then uncomment and eval in cider once pagee has loaded.

;;(jsl/safeLoad  (goog.html.legacyconversions/trustedResourceUrlFromString "https://storage.googleapis.com/app.klipse.tech/plugin_prod/js/klipse_plugin.min.js"))
